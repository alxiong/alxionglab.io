export default [
  {
    icon: "⛓️",
    title: "Bytecode Verifier",
    source: "https://github.com/ConsenSys/bytecode-verifier",
    description: "CLI: Verify Solidity's bytecode against the blockchain."
  },
  {
    icon: "🔓",
    title: "Cryptopals",
    source: "https://github.com/alxiong/cryptopal",
    description: "Cryptopal Puzzle solutions in Rust."
  },
  {
    icon: "🎤",
    title: "Alexa Skill",
    source: "https://github.com/alxiong/alexa-skill-with-arduino-webclient",
    description: "Voice-controlled Arduino."
  },
  {
    icon: "🐹",
    title: "TouchBar Gopher",
    source: "https://github.com/Lancerchiang/TouchBarGopher",
    description: "Whack-a-gopher on your touchbar, a time-killer."
  }
];
