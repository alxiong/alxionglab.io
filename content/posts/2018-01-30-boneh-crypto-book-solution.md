---
date: 2018-01-30
title: 'Boneh Crypto Book Consumption'
template: post
thumbnail: '../thumbnails/writing.png'
slug: boneh-crypto-book-consumption
categories:
  - Crypto
tags:
  - book
---

This is a collection of my _digisted notes_ and _attempted solutions_ to exercises at the end of each chapter in [A Graduate Course in Applied Cryptography](http://toc.cryptobook.us/) by Dan Boneh and Victor Shoup.

### Chapter 0: Number Theory and Abstract Algebra Basics

- 📄[Boneh's book Appendix A](../files/boneh_appendix_a.pdf)
- 📄[Intro to Groups, Rings and Fields](../files/number_theory_intro.pdf)

### Chapter 2: Encryption

- 📄[solution](../files/chapter2_solution.pdf)

### Chapter 3&4

(TODO: I wrote these solutions on papers, and I'll upload them into more readable format when I'm more available, under the assumption that they are not blown away by tropical wind.)

### Chapter 5: Chosen Plaintext Attack

- 📄[solution](../files/Chapter5_exercise.pdf)

### Chapter 6: Message Integrity

- 📄[solution](../files/Chapter6_exercise.pdf)
- 📄[notes](../files/Chapter6_note.pdf)

### Chapter 7: Message Integrity from Universal Hashing

- 📄[solution](../files/Chapter7_exercise.pdf)
- 📄[notes](../files/Chapter7_note.pdf)

### Chapter 10: Public Key Tool

- 📄[solution](../files/Chapter10_exercise.pdf)

> TODO: to be updated