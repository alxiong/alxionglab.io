---
title: About me
slug: about
template: page
---

I’m Alex Xiong, a software engineer 👨‍💻 specialized in applied cryptography 🔐.

I work on privacy-enhancing technologies and blockchain. What drives me
everyday is the hope to gradually **narrow the gap between the promise of
cryptography and the reality of it**. What concerns me the most is the chaotic status
quo of _data ownership_ and _digital privacy_, and their implications on
individual autonomy and collective governance.

I enjoy reading and thinking about irrationality in conventional wisdom,
thence my predilection for contrarian dispositions.

Once at [IBM](https://ibm.com), [ConsenSys](https://consensys.net), [Kyber](https://kyber.network), and [Blockchain at NTU](https://clubs.ntu.edu.sg/ntublockchain/).

## Contact

[GitHub](https://github.com/alxiong) |
[Twitter](https://twitter.com/alex_xiong_) |
[LinkedIn](https://www.linkedin.com/in/luoyuanxiong/)

Email: `hi AT alexxiong DOT com`
