# alexxiong.com

[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)

Alex's personal website running on Gatsby, React, and Node.js.

## Acknowledgements

- Tania Rascia - [taniarasica.com](https://www.taniarascia.com)
- Ruben Harutyunyan - [Gatsby Advanced Starter](https://github.com/vagr9k/gatsby-advanced-starter/)
- Muhammad Muhsin - [Using React Context API with Gatsby](https://www.gatsbyjs.org/blog/2019-01-31-using-react-context-api-with-gatsby/)
- Thomas Frössman - [ExitWP](https://github.com/thomasf/exitwp) - WordPress XML to Markdown

## License

This project is open source and available under the [MIT License](LICENSE).
